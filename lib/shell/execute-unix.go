// +build !windows

package shell

import (
	"errors"
	"github.com/sirupsen/logrus"
	"os/exec"
)

func Execute(command, workFolder string) (text string, err error) {
	logrus.WithFields(logrus.Fields{"command": command, "workFolder": workFolder}).Debug("Utils.execute")
	shell := "/bin/sh"
	flag := "-c"
	cmd := exec.Command(shell, flag, command)
	cmd.Dir = workFolder
	out, err := cmd.CombinedOutput()
	text = string(out)
	if err != nil {
		logrus.WithError(err).WithFields(logrus.Fields{"command": command, "workFolder": workFolder, "text": text}).Error("Utils.execute")
		err = errors.New("err:" + err.Error() + " out:" + text)
		return
	}
	logrus.WithFields(logrus.Fields{"command": command, "workFolder": workFolder, "text": text}).Debug("Utils.execute")
	return
}
