#!/usr/bin/env bash
source ./.env
if ! [ -f /etc/letsencrypt/cli.ini ]; then
    tee /etc/letsencrypt/cli.ini <<EOF
authenticator = webroot
webroot-path = ${APP}/
agree-tos = True
email = $EMAIL
text = True
quiet = True
EOF
fi
