package lib

import (
	"github.com/robfig/cron/v3"
	"github.com/sirupsen/logrus"
	"gitlab.com/shadowy/go/nginx-docker/lib/letsencrypt"
)

// CronUtils - execute tasks
type CronUtils struct {
	cron   *cron.Cron
	entity cron.EntryID
	utils  *letsencrypt.Utils
}

// CreateCron - create cron
func CreateCron(utils *letsencrypt.Utils) error {
	t := CronUtils{utils: utils}
	return t.init()
}

func (util *CronUtils) init() error {
	logrus.Info("CronUtils.Init")
	res := CronUtils{cron: cron.New()}
	var err error
	res.entity, err = res.cron.AddFunc("0 0 * * *", util.letsEncrypt)
	if err != nil {
		logrus.WithError(err).Error("CronUtils.Init")
	}
	res.cron.Start()
	return err
}

func (util *CronUtils) letsEncrypt() {
	logrus.Info("CronUtils.letsEncrypt")
	_, _ = util.utils.Renew()
}
