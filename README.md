# Nginx reverse proxy
###### ver 1.0

docker network create --driver overlay proxy

docker stack deploy --compose-file docker-compose.yml test

certbot certonly --webroot -d queue.bulksms.com.ua

### Labels

* nginx.upstream - name for upstream
* nginx.domain - domain for application
* nginx.grpc - proxy for grpc
* nginx.useHTTPS - switch on https for this domain (default: false)
* nginx.path - path for upstream
* nginx.rewrite - rewrite for nginx
* nginx.network - what network should use for this application (default: proxy)
* nginx.extraDomains - extra domains
* nginx.port (default: 80)
* nginx.protocol (default: http) - what container produce (http, https, grpc)

### Example

```yaml
  queue:
    image: rabbitmq:management
    environment:
      RABBITMQ_DEFAULT_USER: queue
      RABBITMQ_DEFAULT_PASS: queue
    volumes:
      - /opt/docker/test/rabbitmq:/var/lib/rabbitmq
    ports:
      - 15672:15672
      - 5672:5672
    labels:
      - nginx.upstream=test-logs-view
      - nginx.domain=setup.local
      - nginx.port=15672      
      - nginx.path=/queue
      - nginx.extraDomains=
      - nginx.rewrite=/queue/(.*) /$$1  break;
    deploy:
      mode: replicated
      replicas: 0
```

### Environment
* $EMAIL - for letsencrypt
* $DHPARAM_BITS
