module gitlab.com/shadowy/go/nginx-docker

go 1.12

require (
	github.com/alecthomas/template v0.0.0-20160405071501-a0175ee3bccc
	github.com/fsouza/go-dockerclient v1.4.2
	github.com/gin-gonic/gin v1.4.0
	github.com/pkg/errors v0.8.1
	github.com/robfig/cron/v3 v3.0.1
	github.com/sirupsen/logrus v1.4.2
	github.com/swaggo/files v0.0.0-20190704085106-630677cd5c14
	github.com/swaggo/gin-swagger v1.2.0
	github.com/swaggo/swag v1.5.1
	github.com/toorop/gin-logrus v0.0.0-20190701131413-6c374ad36b67
	gitlab.com/shadowy/go/application-settings v1.0.11
	gitlab.com/shadowy/go/logrus-settings v1.0.3
	gitlab.com/shadowy/go/micro-service v1.0.9 // indirect
)
