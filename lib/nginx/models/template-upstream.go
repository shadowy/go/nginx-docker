package models

import "gitlab.com/shadowy/go/nginx-docker/lib/docker/models"

// TemplateUpstream - data for generate upstream
type TemplateUpstream struct {
	Name string
	List []*models.UpstreamContainer
}
