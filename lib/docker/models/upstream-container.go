package models

// UpstreamContainer - information about upstream
type UpstreamContainer struct {
	ContainerID  string
	Upstream     string
	Domain       string
	ExtraDomains string
	Path         string
	Rewrite      string
	Port         int
	Network      string
	IP           string
	UseHTTPS     bool
	Protocol     string
	GRPC         bool
}
