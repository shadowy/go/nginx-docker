package jsonmodel

// ResponseText - Text response for success
type ResponseText struct {
	// Text for operation
	Text string `json:"text"`
}
