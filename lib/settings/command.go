package settings

// Command - data for command
type Command struct {
	Script     string `yaml:"script"`
	WorkFolder string `yaml:"workFolder"`
}
