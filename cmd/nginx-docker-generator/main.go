package main

import (
	"flag"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"github.com/swaggo/files"
	"github.com/swaggo/gin-swagger"
	"github.com/toorop/gin-logrus"
	"gitlab.com/shadowy/go/application-settings"
	_ "gitlab.com/shadowy/go/nginx-docker/docs"
	"gitlab.com/shadowy/go/nginx-docker/lib"
	"gitlab.com/shadowy/go/nginx-docker/lib/docker"
	"gitlab.com/shadowy/go/nginx-docker/lib/letsencrypt"
	"gitlab.com/shadowy/go/nginx-docker/lib/nginx"
	nginxSettings "gitlab.com/shadowy/go/nginx-docker/lib/settings"
	"gitlab.com/shadowy/go/nginx-docker/lib/web-api"
)

func main() {
	logrus.StandardLogger().SetLevel(logrus.DebugLevel)
	var configFile = flag.String("config", "nginx-docker-generator.yml", "file with configuration")
	flag.Parse()

	if *configFile == "" {
		logrus.Warn("file with configuration not defined")
		return
	}
	c, err := settings.ApplicationSettings.FromFile(&nginxSettings.Configuration{}, *configFile)
	if err != nil {
		logrus.WithError(err).WithFields(logrus.Fields{"config": *configFile}).Error("can't load settings")
		return
	}
	config := c.(*nginxSettings.Configuration)

	nginxUtils := nginx.CreateUtils(config.Nginx)
	letsEncryptUtils := letsencrypt.CreateUtils(config.LetsEncrypt)

	_, err = docker.CreateDockerUtilsChannel(nginxUtils, letsEncryptUtils)
	if err != nil {
		logrus.WithError(err).Error("CreateDockerUtilsChannel")
		return
	}

	err = lib.CreateCron(letsEncryptUtils)
	if err != nil {
		logrus.WithError(err).Error("CronUtils")
		return
	}

	logrus.Info("web-api starting ...")
	router := gin.Default()
	log := logrus.New()

	router.Use(ginlogrus.Logger(log), gin.Recovery())
	webapi.CreateNginxController(router, nginxUtils)
	webapi.CreateLetsEncryptController(router, letsEncryptUtils)

	logrus.Info("... web-api started")

	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	err = router.Run(config.Application.Bind)
	if err != nil {
		logrus.WithError(err).Error("router.Run")
		return
	}
}
