package webapi

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/shadowy/go/nginx-docker/lib/letsencrypt"
	"gitlab.com/shadowy/go/nginx-docker/lib/web-api/json-model"
	"net/http"
)

// LetsEncryptController - action for lets encrypt route
type LetsEncryptController struct {
	utils *letsencrypt.Utils
}

var letsEncryptController LetsEncryptController

// CreateLetsEncryptController - create route for control lets encrypt
func CreateLetsEncryptController(engine *gin.Engine, utils *letsencrypt.Utils) {
	router := engine.Group("/api/letsencrypt")
	letsEncryptController = LetsEncryptController{utils: utils}
	router.GET("/create", letsEncryptController.create)
	router.GET("/create-test", letsEncryptController.createTest)
	router.GET("/renew", letsEncryptController.renew)
	router.GET("/renew-test", letsEncryptController.renewTest)
}

// @Description Create certificate
// @Tags LetsEncrypt
// @Accept  json
// @Produce  json
// @Param   domain     query    string     true        "Domain"
// @Success 200	{object} jsonmodel.ResponseText "Success"
// @Failure 500 {object} jsonmodel.ResponseError "Internal error"
// @Router /api/letsencrypt/create [get]
func (controller LetsEncryptController) create(context *gin.Context) {
	res, err := controller.utils.Create(context.Query("domain"))
	if err != nil {
		context.JSON(http.StatusInternalServerError, jsonmodel.ResponseError{Code: "internal", Text: err.Error()})
		return
	}
	context.JSON(http.StatusOK, jsonmodel.ResponseText{Text: res})
}

// @Description Test create certificate
// @Tags LetsEncrypt
// @Accept  json
// @Produce  json
// @Param   domain     query    string     true        "Domain"
// @Success 200	{object} jsonmodel.ResponseText "Success"
// @Failure 500 {object} jsonmodel.ResponseError "Internal error"
// @Router /api/letsencrypt/create-test [get]
func (controller LetsEncryptController) createTest(context *gin.Context) {
	res, err := controller.utils.CreateTest(context.Query("domain"))
	if err != nil {
		context.JSON(http.StatusInternalServerError, jsonmodel.ResponseError{Code: "internal", Text: err.Error()})
		return
	}
	context.JSON(http.StatusOK, jsonmodel.ResponseText{Text: res})
}

// @Description Renew certificates
// @Tags LetsEncrypt
// @Accept  json
// @Produce  json
// @Success 200	{object} jsonmodel.ResponseText "Success"
// @Failure 500 {object} jsonmodel.ResponseError "Internal error"
// @Router /api/letsencrypt/renew [get]
func (controller LetsEncryptController) renew(context *gin.Context) {
	res, err := controller.utils.Renew()
	if err != nil {
		context.JSON(http.StatusInternalServerError, jsonmodel.ResponseError{Code: "internal", Text: err.Error()})
		return
	}
	context.JSON(http.StatusOK, jsonmodel.ResponseText{Text: res})
}

// @Description Test renew certificates
// @Tags LetsEncrypt
// @Accept  json
// @Produce  json
// @Success 200	{object} jsonmodel.ResponseText "Success"
// @Failure 500 {object} jsonmodel.ResponseError "Internal error"
// @Router /api/letsencrypt/renew-test [get]
func (controller LetsEncryptController) renewTest(context *gin.Context) {
	res, err := controller.utils.RenewTest()
	if err != nil {
		context.JSON(http.StatusInternalServerError, jsonmodel.ResponseError{Code: "internal", Text: err.Error()})
		return
	}
	context.JSON(http.StatusOK, jsonmodel.ResponseText{Text: res})
}
