package models

// TemplateSite - data for generate site
type TemplateSite struct {
	Domain       string
	ExtraDomains string
	HTTPPort     int
	HTTPSPort    int
	UseHTTPS     bool
	GRPC         bool
	List         []*TemplateLocation
}
