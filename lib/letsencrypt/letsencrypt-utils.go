package letsencrypt

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"gitlab.com/shadowy/go/nginx-docker/lib/settings"
	"gitlab.com/shadowy/go/nginx-docker/lib/shell"
	"os"
)

// Utils - utils for lets encrypt
type Utils struct {
	settings *settings.LetsEncrypt
}

// CreateUtils - create lets encrypt utils
func CreateUtils(cfg *settings.LetsEncrypt) *Utils {
	res := Utils{settings: cfg}
	return &res
}

// CheckExist - check exists certificates for the domain
func (utils *Utils) CheckExist(domain string) bool {
	if _, err := os.Stat(fmt.Sprintf("/etc/letsencrypt/live/%s", domain)); os.IsNotExist(err) {
		return false
	}
	return true
}

// Create - get certificate for Domain
func (utils *Utils) Create(domain string) (text string, err error) {
	logrus.WithField("domain", domain).Info("letsencrypt.Utils.Create")
	text, err = shell.Execute(utils.settings.Create.Script+domain, utils.settings.CreateTest.WorkFolder)
	return
}

// CreateTest - test get certificate for Domain
func (utils *Utils) CreateTest(domain string) (text string, err error) {
	logrus.WithField("domain", domain).Info("letsencrypt.Utils.CreateTest")
	text, err = shell.Execute(utils.settings.CreateTest.Script+domain, utils.settings.CreateTest.WorkFolder)
	return
}

// Renew - renew certificates
func (utils *Utils) Renew() (text string, err error) {
	logrus.Info("letsencrypt.Utils.Renew")
	text, err = shell.Execute(utils.settings.Renew.Script, utils.settings.RenewTest.WorkFolder)
	return
}

// RenewTest - test renew certificates
func (utils *Utils) RenewTest() (text string, err error) {
	logrus.Info("letsencrypt.Utils.RenewTest")
	text, err = shell.Execute(utils.settings.RenewTest.Script, utils.settings.RenewTest.WorkFolder)
	return
}
