package models

// TemplateLocation - data for location
type TemplateLocation struct {
	Path     string
	Rewrite  string
	Upstream string
	Protocol string
}
