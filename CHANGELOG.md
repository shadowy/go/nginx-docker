# CHANGELOG

<!--- next entry here -->

## 1.3.7
2021-03-14

### Fixes

- add server_names_hash_bucket_size  64 (cfc57e1be7831d1f654d8adda1dd4b15baaf56c7)

## 1.3.6
2021-03-14

### Fixes

- add server_names_hash_bucket_size  64 (48eb3784b3bab480fc4d8975db284a7e10a00688)

## 1.3.5
2021-03-14

### Fixes

- add server_names_hash_bucket_size  64 (236841219860ed7095a2819a6849d7664f405816)

## 1.3.4
2020-07-31

### Fixes

- fix template for grpc (11e20edf8be870375c7dcb73811e5a4e1c75bda0)

## 1.3.3
2020-07-31

### Fixes

- fix template issue (9de819839f8ba160e61e2b553dbd063edc599707)

## 1.3.2
2020-07-31

### Fixes

- fix Dockerfile (3391ede68d9f8575d0091893bb4c6eaf10368b32)

## 1.3.1
2020-07-31

### Fixes

- fix Dockerfile (410e94f28f02f2581722a25a1435441940b3e5fe)

## 1.3.0
2020-07-31

### Features

- add grpc support (b30642e167a148b0d48bfb15e9a761476a9dd0cc)

### Fixes

- switch off lint (5f4f2626ed836a7f78e6610bbf718418d8730033)
- add docs for build (ca26e4e95e088ed5317c1e4b8335a1685dd02da7)

## 1.2.0
2020-01-12

### Features

- add grpc proxy (b949ec394808dde71124b3bd97dc99569970f2f8)

### Fixes

- start cron (e133d4b5231a4bac163df216963678ef03ef1917)
- update cron library (cc119d5b2728be49e2efee9e8db3f89a419a46c5)

## 1.1.3
2019-12-20

### Fixes

- build docker image (545ca125c2acd67694ca795353f2fc8b5e7eced7)

## 1.1.2
2019-12-20

### Fixes

- update makefile for ci (67abf1dcf14b48b6281b3cdf5458930e7c9b553e)

## 1.1.1
2019-12-20

### Fixes

- update ci (478ba155c59cdac96831ec2abf01ee6547161f65)

## 1.1.0
2019-12-20

### Features

- add http2 and update ci (05665103465f0d354027321660a86c9137458a07)

### Fixes

- fix ci (696b730bc5fab49dd3882b5f61b21e6f39793543)