package settings

// LetsEncrypt - settings
type LetsEncrypt struct {
	Create     Command `yaml:"create"`
	CreateTest Command `yaml:"createTest"`
	Renew      Command `yaml:"renew"`
	RenewTest  Command `yaml:"renewTest"`
}
