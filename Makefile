version :=`git describe --abbrev=0 --tags $(git rev-list --tags --max-count=1)`
dockerfile = $(subst .docker,,$@)

all: test lint-full

test:
	@echo ">> test"
	#@go test -coverprofile cover.out
	#@go tool cover -html=cover.out -o cover.html

lint-full: lint card

lint-badge-full: lint card card-badge

card:
	@echo ">> card"
	#@goreportcard-cli -v

lint:
	@echo ">> lint"
	#@golangci-lint run

card-badge:
	@echo ">>card-badge"
	@curl -X GET 'https://goreportcard.com/checks?repo=gitlab.com/shadowy/go/application-settings'

build: nginx-docker-generator

swagger:
	@echo ">>swagger"
	@swag init -g ./cmd/nginx-docker-generator/main.go

nginx-docker-generator:
	@echo ">>build: nginx-docker-generator $(version)"
	@mkdir -p ./dist
	@env GO111MODULE=on CGO_ENABLED=0 GOOS=linux go build -mod=vendor -a -installsuffix cgo -o ./dist/$@ ./cmd/$@/main.go
	@chmod 777 ./dist/$@

docker:
	@echo ">>docker"
	docker build --file ./docker/Dockerfile . -t test-nginx

docker-release: docker-image

docker-login:
	@echo ">>docker-login"
	echo "${REGISTRY_PASSWORD}" | docker login -u "${REGISTRY_USER}" --password-stdin

docker-image:
	@echo ">>docker-image"
	docker build --file ./docker/Dockerfile . -t shadowy/docker-nginx-generator:$(version)
	docker push shadowy/docker-nginx-generator:$(version)

release: docker-login docker-release
