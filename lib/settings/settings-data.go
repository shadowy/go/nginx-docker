package settings

import (
	"gitlab.com/shadowy/go/logrus-settings"
)

// Configuration - application configuration
type Configuration struct {
	Logger      *logsettings.Settings `yaml:"log"`
	Nginx       *Nginx                `yaml:"nginx"`
	LetsEncrypt *LetsEncrypt          `yaml:"letsencrypt"`
	Application *Application          `yaml:"application"`
}
