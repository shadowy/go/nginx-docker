// +build windows

package shell

import (
	"github.com/sirupsen/logrus"
	"os/exec"
)

// Execute - execute shell command
func Execute(command, workFolder string) (text string, err error) {
	logrus.WithFields(logrus.Fields{"command": command, "workFolder": workFolder}).Debug("Utils.execute")
	shell := "cmd"
	flag := "/C"
	cmd := exec.Command(shell, flag, command)
	cmd.Dir = workFolder
	out, err := cmd.CombinedOutput()
	text = string(out)
	if err != nil {
		logrus.WithError(err).WithFields(logrus.Fields{"command": command, "workFolder": workFolder, "text": text}).Error("Utils.execute")
		return
	}
	logrus.WithFields(logrus.Fields{"command": command, "workFolder": workFolder, "text": text}).Debug("Utils.execute")
	return
}
