package docker

import (
	"errors"
	"github.com/fsouza/go-dockerclient"
	"github.com/sirupsen/logrus"
	"gitlab.com/shadowy/go/nginx-docker/lib/docker/models"
	"gitlab.com/shadowy/go/nginx-docker/lib/letsencrypt"
	"gitlab.com/shadowy/go/nginx-docker/lib/nginx"
	"strconv"
	"strings"
	"sync"
)

const labelNginxUpstream = "nginx.upstream"

// UtilsChannel - docker channel
type UtilsChannel struct {
	docker      *docker.Client
	events      chan *docker.APIEvents
	template    nginx.Templates
	nginx       *nginx.Utils
	letsEncrypt *letsencrypt.Utils
	buildSites  map[string]bool
	lock        sync.Mutex
}

// CreateDockerUtilsChannel - create docker channel
func CreateDockerUtilsChannel(nginxUtils *nginx.Utils, letsEncrypt *letsencrypt.Utils) (res *UtilsChannel, err error) {
	logrus.Info("Get docker channel")
	d, err := docker.NewClientFromEnv()
	if err != nil {
		logrus.WithError(err).Error("Connect to docker")
		return
	}
	res = &UtilsChannel{
		docker:      d,
		events:      make(chan *docker.APIEvents),
		template:    nginx.Templates{},
		nginx:       nginxUtils,
		buildSites:  make(map[string]bool),
		letsEncrypt: letsEncrypt,
	}
	_ = d.AddEventListener(res.events)
	res.refresh(nil)
	go res.worker()
	return
}

// worker - catch all event from docker and update upstreams
func (channel *UtilsChannel) worker() {
	for event := range channel.events {
		if event.Type == "container" {
			logrus.WithFields(logrus.Fields{"id": event.ID, "type": event.Type, "action": event.Action}).
				Debug("event")
			switch event.Action {
			case "start":
				channel.refreshForContainer(event.ID)
			case "stop":
				channel.refresh(nil)
			}
		}
	}
}

// refreshForContainer - apply changes when container added
func (channel *UtilsChannel) refreshForContainer(id string) {
	logrus.WithFields(logrus.Fields{"id": id}).Debug("docker.UtilsChannel.refreshForContainer")
	container, err := channel.docker.InspectContainer(id)
	if err != nil {
		logrus.WithError(err).WithFields(logrus.Fields{"id": id}).Debug("refreshForContainer.inspectContainer")
		return
	}
	channel.refresh(&container.ID)
}

// refresh - refresh upstreams
func (channel *UtilsChannel) refresh(containerID *string) {
	logrus.Debug("docker.UtilsChannel.refresh")
	list, err := channel.getAllUpstreams()
	if err != nil {
		return
	}
	err = channel.setIPForUpstreams(list)
	if err != nil {
		return
	}
	workList := channel.filterUpstreamByContainerID(list, containerID)
	var upstreamKeys = make(map[string]bool)
	var siteKeys = make(map[string]bool)
	for i := range workList {
		c := list[i]
		upstreamKeys[c.Upstream] = true
		siteKeys[c.Domain] = true
	}
	for key := range upstreamKeys {
		channel.buildUpstream(list, key)
	}
	for key := range siteKeys {
		channel.buildSite(list, key)
	}
	_, _ = channel.nginx.Reload()
}

// getDomainForContainer - get domain for container
func (channel *UtilsChannel) getDomainForContainer(container *docker.APIContainers, suffix string) (name string) {
	name, res := container.Labels["nginx.domain"+suffix]
	if !res {
		logrus.WithFields(logrus.Fields{"id": container.ID, "names": container.Names}).
			Debug("getDomainForContainer container has not needed information")
	}
	return
}

// getDomainForContainer - get domain for container
func (channel *UtilsChannel) getExtraDomainForContainer(container *docker.APIContainers, suffix string) (name string) {
	name, res := container.Labels["nginx.extraDomains"+suffix]
	if !res {
		logrus.WithFields(logrus.Fields{"id": container.ID, "names": container.Names}).
			Debug("getDomainForContainer container has not needed information")
	}
	return
}

// getForGRPC - get GRPC flag for container
func (channel *UtilsChannel) getForGRPC(container *docker.APIContainers, suffix string) (res bool) {
	name, res := container.Labels["nginx.grpc"+suffix]
	if !res {
		return false
	}
	return strings.ToLower(name) == "true"
}

// getNetworkForContainer - get network for container
func (channel *UtilsChannel) getNetworkForContainer(container *docker.APIContainers, suffix string) (name string) {
	name, res := container.Labels["nginx.network"+suffix]
	if !res {
		return "proxy"
	}
	return
}

// getPathForContainer - get path for container
func (channel *UtilsChannel) getPathForContainer(container *docker.APIContainers, suffix string) (name string) {
	name, res := container.Labels["nginx.path"+suffix]
	if !res {
		return "/"
	}
	return
}

// getPortForContainer - get port for container
func (channel *UtilsChannel) getPortForContainer(container *docker.APIContainers, suffix string) (port int) {
	text, res := container.Labels["nginx.port"+suffix]
	if !res {
		return nginx.DefaultHTTP
	}
	port, err := strconv.Atoi(text)
	if err != nil {
		port = 0
		logrus.WithError(err).WithFields(logrus.Fields{
			"containerId":    container.ID,
			"image":          container.Image,
			"nginx.upstream": container.Labels[labelNginxUpstream+suffix],
			"suffix":         suffix,
		}).Error("channel.getPortForContainer")
	}
	return
}

// getPortForContainer - get rewrite for container
func (channel *UtilsChannel) getRewriteForContainer(container *docker.APIContainers, suffix string) string {
	text, res := container.Labels["nginx.rewrite"+suffix]
	if !res {
		return ""
	}
	return text
}

// getPortForContainer - get rewrite for container
func (channel *UtilsChannel) getUseHTTPSForContainer(container *docker.APIContainers, suffix string) bool {
	text, res := container.Labels["nginx.useHTTPS"+suffix]
	if !res {
		return false
	}
	return text == "true"
}

// getProtocolForContainer - get protocol for container
func (channel *UtilsChannel) getProtocolForContainer(container *docker.APIContainers, suffix string) string {
	text, res := container.Labels["nginx.protocol"+suffix]
	if !res {
		return "http"
	}
	return text
}

// getSuffix - get list of suffix for container
func (channel *UtilsChannel) getSuffix(container *docker.APIContainers) []string {
	list := make([]string, 0)
	if _, ok := container.Labels[labelNginxUpstream]; ok {
		list = append(list, "")
	}
	index := 0
	name, ok := container.Labels[labelNginxUpstream+"."+strconv.Itoa(index)]
	for ok {
		list = append(list, name[len(labelNginxUpstream):])
		index++
		name, ok = container.Labels[labelNginxUpstream+"."+strconv.Itoa(index)]
	}
	return list
}

// getUpstreamsFromContainer - get upstreams for container
func (channel *UtilsChannel) getUpstreamsFromContainer(container *docker.APIContainers) (list []*models.UpstreamContainer) {
	suffixes := channel.getSuffix(container)
	if len(suffixes) == 0 {
		return
	}
	for _, suffix := range suffixes {
		data := models.UpstreamContainer{
			ContainerID:  container.ID,
			Upstream:     strings.Trim(container.Labels[labelNginxUpstream+suffix], " "),
			Domain:       strings.Trim(channel.getDomainForContainer(container, suffix), " "),
			ExtraDomains: strings.Trim(channel.getExtraDomainForContainer(container, suffix), " "),
			Path:         channel.getPathForContainer(container, suffix),
			Rewrite:      channel.getRewriteForContainer(container, suffix),
			Network:      channel.getNetworkForContainer(container, suffix),
			Port:         channel.getPortForContainer(container, suffix),
			UseHTTPS:     channel.getUseHTTPSForContainer(container, suffix),
			Protocol:     channel.getProtocolForContainer(container, suffix),
			GRPC:         channel.getForGRPC(container, suffix),
		}
		list = append(list, &data)
	}
	return list
}

// getAllUpstreams - get information from all containers
func (channel *UtilsChannel) getAllUpstreams() (list []*models.UpstreamContainer, err error) {
	containers, err := channel.docker.ListContainers(docker.ListContainersOptions{All: false})
	if err != nil {
		logrus.WithError(err).Error("getAllUpstreams")
		return
	}
	for i := range containers {
		container := &containers[i]
		upstreams := channel.getUpstreamsFromContainer(container)
		list = append(list, upstreams...)
	}
	return list, err
}

// filterUpstreamByContainerID - apply containerID filter to list
func (channel *UtilsChannel) filterUpstreamByContainerID(list []*models.UpstreamContainer,
	containerID *string) []*models.UpstreamContainer {
	var result []*models.UpstreamContainer
	if containerID == nil {
		return list
	}
	for _, upstream := range list {
		if upstream.ContainerID == *containerID {
			result = append(result, upstream)
		}
	}
	return result
}

// filterUpstreamByName - apply name filter to list
func (channel *UtilsChannel) filterUpstreamByName(list []*models.UpstreamContainer, name string) []*models.UpstreamContainer {
	var result []*models.UpstreamContainer
	for _, upstream := range list {
		if upstream.Upstream == name {
			result = append(result, upstream)
		}
	}
	return result
}

// filterUpstreamByDomain - apply domain filter to list
func (channel *UtilsChannel) filterUpstreamByDomain(list []*models.UpstreamContainer, name string) []*models.UpstreamContainer {
	var result []*models.UpstreamContainer
	for _, upstream := range list {
		if upstream.Domain == name {
			result = append(result, upstream)
		}
	}
	return result
}

// getNetworks - get all networks from docker
func (channel *UtilsChannel) getNetworks() (networksData map[string]map[string]docker.Endpoint, err error) {
	networks, err := channel.docker.ListNetworks()
	if err != nil {
		logrus.WithError(err).Error("getNetworks.NetworkList")
		return
	}
	networksData = make(map[string]map[string]docker.Endpoint)
	for i := range networks {
		network := networks[i]
		var data *docker.Network
		data, err = channel.docker.NetworkInfo(network.ID)
		if err != nil {
			logrus.WithError(err).Error("getNetworks.NetworkInspect")
			return
		}
		networksData[network.Name] = data.Containers
	}
	return
}

// setIPForUpstreams - set ip address for each upstream
func (channel *UtilsChannel) setIPForUpstreams(list []*models.UpstreamContainer) (err error) {
	networkData, err := channel.getNetworks()
	if err != nil {
		return
	}
	for _, item := range list {
		if n, ok := networkData[item.Network]; ok {
			if e, ok := n[item.ContainerID]; ok {
				t := strings.Split(e.IPv4Address, "/")
				item.IP = t[0]
			} else {
				err = errors.New("not found container in network with name: " + item.Network + " container: " + item.ContainerID)
				logrus.WithFields(logrus.Fields{"networkName": item.Network, "containerId": item.ContainerID}).
					WithError(err).Error("setIPForUpstreams.SetIpFindContainer")
				return
			}
		} else {
			err = errors.New("not found network with name: " + item.Network + " for container: " + item.ContainerID)
			logrus.WithFields(logrus.Fields{"networkName": item.Network, "containerId": item.ContainerID}).
				WithError(err).Error("setIPForUpstreams.SetIpFindNetwork")
			return
		}
	}
	return
}

// buildUpstream - update upstream for nginx
func (channel *UtilsChannel) buildUpstream(list []*models.UpstreamContainer, name string) {
	logrus.WithFields(logrus.Fields{"name": name}).Debug("docker.UtilsChannel.buildUpstream")
	listForGenerate := channel.filterUpstreamByName(list, name)
	if len(listForGenerate) == 0 {
		return
	}
	text := channel.template.ExecuteUpstream(name, listForGenerate)
	_ = channel.nginx.WriteUpstreams(name, text)
}

// buildSite - update site for nginx
func (channel *UtilsChannel) buildSite(list []*models.UpstreamContainer, name string) {
	logrus.WithFields(logrus.Fields{"name": name}).Debug("docker.UtilsChannel.buildSite")
	listForGenerate := channel.filterUpstreamByDomain(list, name)
	if len(listForGenerate) == 0 {
		return
	}
	useHTTPS := false
	for _, stream := range listForGenerate {
		if stream.UseHTTPS {
			useHTTPS = true
			break
		}
	}
	if useHTTPS {
		for _, stream := range listForGenerate {
			stream.UseHTTPS = true
		}
		if !channel.letsEncrypt.CheckExist(name) {
			for _, stream := range listForGenerate {
				stream.UseHTTPS = false
			}
			text := channel.template.ExecuteSite(name, listForGenerate)
			_ = channel.nginx.WriteSite(name, text)

			go func() {
				channel.lock.Lock()
				if v, ok := channel.buildSites[name]; ok && v {
					channel.lock.Unlock()
					return
				}
				channel.buildSites[name] = true
				channel.lock.Unlock()
				channel.buildCertificate(name, listForGenerate)
			}()
			return
		}
	}
	text := channel.template.ExecuteSite(name, listForGenerate)
	_ = channel.nginx.WriteSite(name, text)
}

func (channel *UtilsChannel) buildCertificate(domain string, list []*models.UpstreamContainer) {
	logrus.WithFields(logrus.Fields{"domain": domain}).Debug("docker.UtilsChannel.buildCertificate")

	defer func() {
		channel.lock.Lock()
		channel.buildSites[domain] = false
		channel.lock.Unlock()
	}()

	res, err := channel.letsEncrypt.CreateTest(domain)
	if err != nil {
		logrus.WithError(err).WithFields(logrus.Fields{"domain": domain}).
			Error("docker.UtilsChannel.buildCertificate createTest")
		return
	}
	logrus.WithFields(logrus.Fields{"domain": domain, "text": res}).
		Debug("docker.UtilsChannel.buildCertificate createTest")

	res, err = channel.letsEncrypt.Create(domain)
	if err != nil {
		logrus.WithError(err).WithFields(logrus.Fields{"domain": domain}).
			Error("docker.UtilsChannel.buildCertificate create")
		return
	}
	logrus.WithFields(logrus.Fields{"domain": domain, "text": res}).
		Debug("docker.UtilsChannel.buildCertificate create")

	if channel.letsEncrypt.CheckExist(domain) {
		for _, stream := range list {
			stream.UseHTTPS = true
		}
		text := channel.template.ExecuteSite(domain, list)
		_ = channel.nginx.WriteSite(domain, text)
		res, err = channel.nginx.Reload()
		if err != nil {
			logrus.WithError(err).WithFields(logrus.Fields{"domain": domain, "text": res}).
				Error("docker.UtilsChannel.buildCertificate reload")
		}
	}
}
