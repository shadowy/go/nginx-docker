package nginx

import (
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/shadowy/go/nginx-docker/lib/settings"
	"gitlab.com/shadowy/go/nginx-docker/lib/shell"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
	"strings"
)

// Utils - nginx utils
type Utils struct {
	settings *settings.Nginx
}

// CreateUtils - create nginx utils
func CreateUtils(cfg *settings.Nginx) *Utils {
	res := Utils{settings: cfg}
	return &res
}

// Reload - reload nginx configuration
func (utils Utils) Reload() (text string, err error) {
	logrus.Info("Utils.Reload")
	text, err = shell.Execute(utils.settings.Reload.Script, utils.settings.Reload.WorkFolder)
	return
}

// Test - test nginx configuration
func (utils Utils) Test() (text string, err error) {
	logrus.Info("Utils.Test")
	text, err = shell.Execute(utils.settings.Test.Script, utils.settings.Test.WorkFolder)
	if err != nil {
		return
	}
	if !strings.Contains(text, "test is successful") {
		err = errors.New(text)
	}
	return
}

// WriteUpstreams - write upstream to file
func (utils Utils) WriteUpstreams(filename, text string) (err error) {
	logrus.WithFields(logrus.Fields{"filename": filename}).Debug("Utils.WriteUpstreams")
	return utils.writeConfig(filename, "upstreams", text)
}

// WriteSite - write site to file
func (utils Utils) WriteSite(filename, text string) (err error) {
	logrus.WithFields(logrus.Fields{"filename": filename}).Debug("Utils.WriteSite")
	return utils.writeConfig(filename, "hosts", text)
}

// CheckUpstream - check exist file for upstream or not
func (utils Utils) CheckUpstream(filename string) bool {
	logrus.WithFields(logrus.Fields{"filename": filename}).Debug("Utils.CheckUpstream")
	return utils.checkFile(filename, "upstreams")
}

// CheckSite - check exist file for site or not
func (utils Utils) CheckSite(filename string) bool {
	logrus.WithFields(logrus.Fields{"filename": filename}).Debug("Utils.CheckSite")
	return utils.checkFile(filename, "hosts")
}

// RemoveUpstreams - remove all upstreams
func (utils Utils) RemoveUpstreams() {
	dir := path.Join(utils.settings.ConfigFolder, "/generator/upstreams/")
	d, err := os.Open(dir)
	if err != nil {
		logrus.WithError(err).Error("nginx.Utils.RemoveUpstreams open dir")
		return
	}
	defer func() {
		_ = d.Close()
	}()
	names, err := d.Readdirnames(-1)
	if err != nil {
		logrus.WithError(err).Error("nginx.Utils.RemoveUpstreams read files")
		return
	}
	for _, name := range names {
		err = os.RemoveAll(filepath.Join(dir, name))
		if err != nil {
			logrus.WithFields(logrus.Fields{"file": name}).WithError(err).
				Error("nginx.Utils.RemoveUpstreams remove")
		}
	}
}

func (utils Utils) writeConfig(filename, place, text string) (err error) {
	logrus.WithFields(logrus.Fields{"filename": filename, "place": place}).Debug("Utils.writeConfig")
	err = ioutil.WriteFile(path.Join(utils.settings.ConfigFolder, "/generator/"+place+"/", filename+".conf"), []byte(text), 0600)
	if err != nil {
		logrus.WithError(err).WithFields(logrus.Fields{"upstream": text, "filename": filename}).Error("Utils.writeConfig")
	}
	return
}

func (utils Utils) checkFile(filename, place string) bool {
	logrus.WithFields(logrus.Fields{"filename": filename, "place": place}).Debug("Utils.checkFile")
	file := path.Join(utils.settings.ConfigFolder, "/generator/"+place+"/", filename+".conf")
	if _, err := os.Stat(file); err == nil {
		return true
	}
	return false
}
