#!/usr/bin/env bash
source ./.env
# The hash of the pregenerated dhparam file is used to check if the pregen dhparam is already in use
PREGEN_HASH=$(md5sum $PREGEN_DHPARAM_FILE | cut -d" " -f1)
if [[ -f $DHPARAM_FILE ]]; then
    CURRENT_HASH=$(md5sum $DHPARAM_FILE | cut -d" " -f1)
    if [[ $PREGEN_HASH != $CURRENT_HASH ]]; then
        # There is already a dhparam, and it's not the default
        echo "Custom dhparam.pem file found, generation skipped"
        exit 0
    fi
fi

cat >&2 <<-EOT
WARNING: $DHPARAM_FILE was not found. A pre-generated dhparam.pem will be used for now while a new one
is being generated in the background.  Once the new dhparam.pem is in place, nginx will be reloaded.
EOT

# Put the default dhparam file in place so we can start immediately
echo "cp $PREGEN_DHPARAM_FILE $DHPARAM_FILE"
cp $PREGEN_DHPARAM_FILE $DHPARAM_FILE

echo "openssl dhparam -out $DHPARAM_FILE $DHPARAM_BITS"
# Generate a new dhparam in the background in a low priority and reload nginx when finished (grep removes the progress indicator).
(
    (
        nice -n +5 openssl dhparam -out $DHPARAM_FILE.tmp $DHPARAM_BITS 2>&1 \
        && echo "dhparam generation complete, reloading nginx" \
        && mv $DHPARAM_FILE.tmp $DHPARAM_FILE \
        && nginx -s reload
    ) | grep -vE '^[\.+]+'
) &disown
