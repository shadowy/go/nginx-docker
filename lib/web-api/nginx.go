package webapi

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/shadowy/go/nginx-docker/lib/nginx"
	"gitlab.com/shadowy/go/nginx-docker/lib/web-api/json-model"
	"net/http"
)

// NginxController - action for nginx route
type NginxController struct {
	nginxUtil *nginx.Utils
}

var nginxController NginxController

// CreateNginxController - create route for control nginx
func CreateNginxController(engine *gin.Engine, nginxUtil *nginx.Utils) {
	router := engine.Group("/api/nginx")
	nginxController = NginxController{nginxUtil: nginxUtil}
	router.PATCH("/refresh", nginxController.refresh)
	router.GET("/test", nginxController.test)
}

// @Description Nginx reload settings
// @Tags Nginx
// @Accept  json
// @Produce  json
// @Success 200	{object} jsonmodel.ResponseText "Success"
// @Failure 500 {object} jsonmodel.ResponseError "Internal error"
// @Router /api/nginx/refresh [patch]
func (controller NginxController) refresh(context *gin.Context) {
	res, err := controller.nginxUtil.Reload()
	if err != nil {
		context.JSON(http.StatusInternalServerError, jsonmodel.ResponseError{Code: "internal", Text: err.Error()})
		return
	}
	context.JSON(http.StatusOK, jsonmodel.ResponseText{Text: res})
}

// @Description Nginx test settings
// @Tags Nginx
// @Accept  json
// @Produce  json
// @Success 200	{object} jsonmodel.ResponseText "Success"
// @Failure 500 {object} jsonmodel.ResponseError "Internal error"
// @Router /api/nginx/test [get]
func (controller NginxController) test(context *gin.Context) {
	res, err := controller.nginxUtil.Test()
	if err != nil {
		context.JSON(http.StatusInternalServerError, jsonmodel.ResponseError{Code: "internal", Text: err.Error()})
		return
	}
	context.JSON(http.StatusOK, jsonmodel.ResponseText{Text: res})
}
