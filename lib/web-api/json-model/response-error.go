package jsonmodel

// ResponseError - Common error structure
type ResponseError struct {
	// Error code
	Code string `json:"code"`
	// Error description
	Text string `json:"text"`
}
