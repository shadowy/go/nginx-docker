package settings

// Nginx - settings
type Nginx struct {
	ConfigFolder string   `yaml:"configFolder"`
	Reload       *Command `yaml:"reload"`
	Test         *Command `yaml:"test"`
}
