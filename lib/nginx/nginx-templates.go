package nginx

import (
	"bytes"
	"github.com/sirupsen/logrus"
	dockerModels "gitlab.com/shadowy/go/nginx-docker/lib/docker/models"
	"gitlab.com/shadowy/go/nginx-docker/lib/nginx/models"
	"io/ioutil"
	"path/filepath"
	"text/template"
)

const DefaultHTTP = 80
const DefaultHTTPS = 443

// Templates - utils for execution template from upstream and sites
type Templates struct {
}

// ExecuteUpstream - execute template for upstream
func (tmpl Templates) ExecuteUpstream(upstream string, list []*dockerModels.UpstreamContainer) (res string) {
	logrus.WithFields(logrus.Fields{"upstream": upstream}).Info("nginx.Templates.ExecuteUpstream")
	model := models.TemplateUpstream{Name: upstream, List: list}
	t, err := template.New("upstream").Parse(tmpl.getUpstreamTemplate())
	if err != nil {
		logrus.WithError(err).Error("nginx.Templates.ExecuteUpstream parse")
		return ""
	}
	buf := new(bytes.Buffer)
	if err := t.Execute(buf, model); err != nil {
		logrus.WithError(err).Error("nginx.Templates.ExecuteUpstream execute")
		return ""
	}
	return buf.String()
}

// ExecuteSite - execute template for site
func (tmpl Templates) ExecuteSite(domain string, list []*dockerModels.UpstreamContainer) (res string) {
	logrus.WithFields(logrus.Fields{"domain": domain}).Info("nginx.Templates.ExecuteSite")
	model := models.TemplateSite{
		Domain:       list[0].Domain,
		ExtraDomains: list[0].ExtraDomains,
		HTTPPort:     DefaultHTTP,
		HTTPSPort:    DefaultHTTPS,
		UseHTTPS:     list[0].UseHTTPS,
		GRPC:         list[0].GRPC,
		List:         make([]*models.TemplateLocation, 0),
	}
	locations := make(map[string]*models.TemplateLocation)
	for i := range list {
		upstream := list[i]
		if _, ok := locations[upstream.Path]; ok {
			continue
		}
		locations[upstream.Path] = &models.TemplateLocation{
			Path:     upstream.Path,
			Upstream: upstream.Upstream,
			Rewrite:  upstream.Rewrite,
			Protocol: upstream.Protocol,
		}
	}
	for key := range locations {
		model.List = append(model.List, locations[key])
	}
	t, err := template.New("site").Parse(tmpl.getSiteTemplate(model.GRPC))
	if err != nil {
		logrus.WithError(err).Error("nginx.Templates.ExecuteSite parse")
		return ""
	}
	buf := new(bytes.Buffer)
	if err := t.Execute(buf, model); err != nil {
		logrus.WithError(err).Error("nginx.Templates.ExecuteSite execute")
		return ""
	}
	return buf.String()
}

// getUpstreamTemplate - get template for upstream
func (tmpl Templates) getUpstreamTemplate() string {
	path, err := filepath.Abs("./template/upstream.tmpl")
	if err != nil {
		logrus.WithError(err).Error("nginx.Templates.getUpstreamTemplate abs")
		return ""
	}
	b, err := ioutil.ReadFile(path)
	if err != nil {
		logrus.WithError(err).Error("nginx.Templates.getUpstreamTemplate")
		return ""
	}
	return string(b)
}

// getSiteTemplate - get template for site
func (tmpl Templates) getSiteTemplate(grpc bool) string {
	var path string
	var err error
	if grpc {
		path, err = filepath.Abs("./template/site-grpc.tmpl")
	} else {
		path, err = filepath.Abs("./template/site.tmpl")
	}
	if err != nil {
		logrus.WithError(err).Error("nginx.Templates.getSiteTemplate abs")
		return ""
	}
	b, err := ioutil.ReadFile(path)
	if err != nil {
		logrus.WithError(err).Error("nginx.Templates.getSiteTemplate")
		return ""
	}
	return string(b)
}
