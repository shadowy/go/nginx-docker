#!/usr/bin/env bash

source ./.env
folders=(dhparam hosts upstreams)
for folder in "${folders[@]}"
do
  if [ ! -d "${GENERATOR}/${folder}" ]; then
    echo "mkdir ${GENERATOR}/${folder}"
    mkdir ${GENERATOR}/${folder}
  fi
done
